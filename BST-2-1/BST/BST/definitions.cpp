#include <iostream>
#include<cstdlib>
#include "stdafx.h"
#include"Classes.h"

using namespace std;


BST::BST()
{
	root = NULL;
}
BST::node* BST::NewLeaf(int data)
{
	node* n = new node;// Create and stores a newly created node in a variable.
	n->data = data;//Sets the data variable inside of the new node equal to whats pasted in.
	n->left = NULL;//Insure the leaf's pointers aren't pointing to anything yet.
	n->right=NULL;

	return n;

}
//Finds the location to insert a new leaf and return a pointer to it.
void BST::AddLeaf(int data)
{
	InsertLeaf(data, root);
}
void BST::InsertLeaf(int data, node* Ptr)
{
	if (root == NULL)//If the tree is empty.
	{
		root = NewLeaf(data);//What's being added is the root.
	}
	else if (data < Ptr->data)//If the value user want's to add is less than the node we are at(The root).
	{
		if (Ptr->left != NULL)//If there is already a node here.
		{
			InsertLeaf(data, Ptr->left);//Go to the next node on this path.
		}
		else
		{
			Ptr->left = NewLeaf(data);//if there isn't already a node here put the new one here.
		}
	}
	//Same as above but on the right side.
	else if (data > Ptr->data)
	{
		if (Ptr->right != NULL)
		{
			InsertLeaf(data, Ptr->right);
		}
		else
		{
			Ptr->right = NewLeaf(data);
		}
	}
	else //If value is equal to something in tree.
	{
		cout << "\n" << data << " already exists in the tree." << endl;
	}
}
	
	
	
	void BST::PrintStart()
	{
		cout << "Here are the vales in the tree listed in order." << endl;
		PrintOrdered(root);
	}

	void BST::PrintOrdered(node* Ptr)
	{
		if (root != NULL)
		{
			if (Ptr->left !=NULL)
			{
				PrintOrdered(Ptr->left);
				cout << Ptr->data << "" << endl;
			}
			cout << Ptr->data << ""<<endl;
			if (Ptr->right != NULL)
			{
				PrintOrdered(Ptr->right);
				cout << Ptr->data << "" << endl;
			}

		}
		else
		{
			cout << "The tree is empty" << endl;
		}
	}

	BST::node* BST::FindNode(int data)
	{
		return ReturnNode(data, root);
	}

	BST::node* BST::ReturnNode(int data, node* Ptr)
	{
		if (Ptr != NULL)//If the passed is pointing to something.
		{
			if (Ptr->data == data)//If we find what we're looking for return the pointer
			{
				return Ptr;
			}
			else//Keep looking through
			{
				if (data < Ptr->data)//If what we're looking for is a lesser value
				{
					return ReturnNode(data, Ptr->left);
				}
				else
				{
					return ReturnNode(data, Ptr->right);//If what we're looking for is of grater value.
				}

			}
		}
		else//If the passed in pointer isn't pointing to anything.
		{
			return NULL;
		}
	}

	int BST::FindMin()
	{
		return BranchFindMin(root);
	}

	int BST::BranchFindMin(node* Ptr)
	{
		if (root = NULL)//If tree is empty.
		{
			cout << "\nThe tree is empty." << endl;
			return -1000;
		}
		else//Tree not empty
		{
			if (Ptr->left !=NULL)//If we're not at the last leaf to the left.
			{
				return BranchFindMin(Ptr->left);//Keep going through.
			}
			else//We have arrived at the smallest.
			{
				return Ptr->data;
			}
		}
	}

	int BST::FindMax()
	{
		return FindMaxBranch(root);
	}

	int BST::FindMaxBranch(node* Ptr)
	{
		if (root = NULL)//If tree is empty.
		{
			cout << "\nThe tree is empty." << endl;
			return -1000;
		}
		else//Tree not empty
		{
			if (Ptr->right != NULL)//If we're not at the last leaf to the left.
			{
				return FindMaxBranch(Ptr->right);//Keep going through.
			}
			else//We have arrived at the largest.
			{
				return Ptr->data;
				cout << "\N The largest value is " << Ptr->data;
			}
		}
	}

	void BST::DeleatLeaf(int data)
	{
		DeleatLeafUtility(data, root);
	}

	void BST::DeleatLeafUtility(int data, node* parent)
	{
		if (root!=NULL)//As long as the tree is not empty.
		{
			if (root->data==data)//If the root is what we want to delete.
			{
				RemoveRoot();
			}
			else
			{
				if (data < parent->data && parent->left!=NULL)//Go down the left side if we can.
				{
					parent->left->data == data ?//If the left node value matches.
						RemoveNode(parent, parent->left, true)://Remove node
						DeleatLeafUtility(data, parent->left);//Move down right side.
				}
				 else if (data > parent->data && parent->right != NULL)//Go down the left side if we can.
				{
					parent->right->data == data ?//If the left node value matches.
						RemoveNode(parent, parent->right, false) ://Remove node
						DeleatLeafUtility(data, parent->right);//Move down right side.
				}
				 else
				 {
					 cout << data << " is not in the tree" << endl;
				 }
			}
		}
		else
		{
			cout << "The tree is empty." << endl;
		}
	}

	void  BST::RemoveRoot()
	{
		if (root!=NULL)//If tree isn't empty
		{
			node* deletePtr = root;
			int nextRootValue;

			if (root->left == NULL&& root->right == NULL)//If the root had no branches
			{//Delete root
				root = NULL;
				delete deletePtr;
			}
			else if (root->left == NULL&&root->right != NULL)//If the root has one branch to the left.
			{//Right node is new root and delete old root
				root = root->right;
				deletePtr->right = NULL;
				delete deletePtr;
				cout << "The root was deleted" << "The new root is" << root->data << endl;
			}
			else if (root->left != NULL&&root->right == NULL)//If the root has one branch to the right.
			{
				root = root->left;
				deletePtr->left = NULL;
				delete deletePtr;
				cout << "The root was deleted" << "The new root is" << root->data << endl;
			}
			else// If root has two branches.
			{
				//Overwrites the root with what the smallest value in the right side.
				nextRootValue = BranchFindMin(root->right);
				DeleatLeafUtility(nextRootValue, root);
				root->data = nextRootValue;
				cout << "The root was deleted" << "The new root is" << root->data << endl;
			}
		}
		else//If tree is empty
		{
			cout << "The tree is empty. there is no root." << endl;
		}
	}

	void BST::RemoveNode(node* parent, node*match, bool left)
	{
		if (root != NULL)
		{
			node* deletePtr;
			int matchData=match->data;
			int nextnodeValue;

			if ( match->left == NULL && match->right == NULL)//If the node we want to remove had no branches.
			{
				deletePtr = match;
				left == true ? parent->left = NULL : parent->right = NULL;
				delete deletePtr;
				cout << "\nThe node was removed" << endl;
			}
			else if (match->left==NULL && match->right!=NULL)
			{
				left == true ? parent->left = match->right : parent->right = match->right;
				match->right = NULL;
				delete match;
				cout << "\nThe node was removed" << endl;
			}
			else if (match->left != NULL && match->right == NULL)
			{
				left == true ? parent->left = match->left : parent->right = match->left;
				match->left = NULL;
				delete match;
				cout << "\nThe node was removed" << endl;
			}
			else// The node we want to delete has two children.
			{
				nextnodeValue = BranchFindMin(match->right);
				DeleatLeafUtility(nextnodeValue, match);
				match->data = nextnodeValue;
				cout << "\nThe node was removed" << endl;
			}
		}
	}

	//makes it so I only need to write code that checks for valid entries once.
		int EnterValid()
	{
		int x;
		cout << "Please enter your value." << endl;
		while (!(cin >> x ||x<0))//Test if the input will give an error.
		{
			cin.clear();//If the input is bad get rid of it.
			cin.ignore();
			cout << "Invalid input. Please enter a value." << endl;
		}
		cin.clear();
		cin.ignore(10000, '\n');
		return x;
	}

char Menu()
{
	char x = 'f';
	do
	{ //Do, try, and catch to check input and insure the program doesn't crash.
		try
		{
			cout << "\nWhat would you like to do?" << endl;
			cout << "1. Add a leaf to the tree?" << endl;
			cout << "2. Remove a leaf from the tree." << endl;
			cout << "3. Print the list." << endl;
			cout << "4. Find what the minimum value the tree." << endl;
			cout << "5. Find the maximum value in the tree." << endl;
			cout << "6. Exit the program." << endl;
			cin >> x;
			cin.ignore();
			//cout << x << endl; Testing input.
			if (x != '1' && x != '2' && x != '3' && x != '4' && x != '5'&& x != '6')
			{
				throw "Catch type char* \nThat was not an option.";
			}
		}
		catch (char * message)
		{
			cout << message << endl;
		}
		catch (...)
		{
			cout << "Error: What you entered was not a proper response." << endl;
		}
	} while (x != '1' && x != '2' && x != '3' && x != '4' && x != '5' && x != '6'&& x != '7');
	return x;


}