#pragma once
#include <iostream>
#include<cstdlib>
#include "stdafx.h"
#include"Classes.h"

using namespace std;

class BST
{
private:
	struct node
	{
		int data;
		node* left;//Pointer to nodes left (with data values less then that of this node) of this one.
		node* right;//Pointer to nodes right (with data values grater then that of this node) of this one.
		
	};

	node* root;//The start of the tree
	//Theses help move through the tree.
	void InsertLeaf(int data,node* Ptr);//insery
	void PrintOrdered(node* Ptr);
	node* ReturnNode(int key, node* Ptr);// Returns desired node.
	int BranchFindMin(node* Ptr);//Find smallest in branch Utility for delete.
	int FindMaxBranch(node* Ptr);//Find the highest value
	void DeleatLeafUtility(int data, node* Ptr);//
	void RemoveRoot();//If the root is what needs to be deleted.
	void RemoveNode(node* parent,node*match,bool left);//Utility for removing 

public:
	
	BST();//Constructor
	node* NewLeaf(int data);//Function that creates a new leafs(node with value).
	void AddLeaf(int data);//add
	void PrintStart();//
	node* FindNode(int data);//Utility finds desired node.
	int FindMin();//Find smallest Utility for delete.
	int FindMax();//Find the highest value
	void DeleatLeaf(int data);//Removes a node from tree
};
//A function that makes it so I only need to write code that checks for valid entries once.
int EnterValid();

//Function that displays and handles the main menu.
char Menu();