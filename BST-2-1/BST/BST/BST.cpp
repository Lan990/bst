// BST.cpp : Defines the entry point for the console application.
//
#include <iostream>
#include<cstdlib>
#include "stdafx.h"
#include"Classes.h"

using namespace std;


int main()
{	BST tree;
	char action;
	cout << "Hello and good afternoon,\n Today we will demonstrate a Binary Search Tree. " << endl;

	
	do
	{
		action = Menu();
		if (action=='1')
		{
			int validValue = EnterValid();
			tree.AddLeaf(validValue);
		}

		if (action=='2')
		{
			tree.PrintStart();
			int validValue = EnterValid();
			tree.DeleatLeaf(validValue);
		}

		if (action=='3')
		{
			tree.PrintStart();
		}
		if ( action=='4')
		{
			tree.FindMin();
		}
		if ( action=='5')
		{
			tree.FindMax();
		}

	} while (action!='6');
	system("Pause");

    return 0;
}

